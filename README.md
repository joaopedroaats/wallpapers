### Download

```sh
git clone git@gitlab.com:joaopedroaats/wallpapers.git ~/Pictures/Wallpapers/
```


# Wallpapers
![image](./preview.png)

If you think you have a similar wallpaper, feel free to send me a pull request!


## Abstract
![image](./Abstract/0001.jpg)

## Artvee
![image](./Artvee/0005.jpg)

## Distro
![image](./Distro/0017.jpg)

## Dual
![image](./Dual/0007.jpg)

## Lake
![image](./Lake/0002.jpg)

## Minimal
![image](./Minimal/0004.jpg)

## Mountain
![image](./Mountain/0042.jpg)

## Purple
![image](./Purple/0012.jpg)

## Tree
![image](./Tree/0007.jpg)

