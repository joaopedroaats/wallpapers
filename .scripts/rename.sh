source ~/dotfiles/.scripts/separate_echo.sh

for folder in */ ; do
  if [ -d "$folder" ]; then
    cd ./$folder
    separate_echo "$folder"

    a=1
    b=1

    for filename in *.*g ; do
      uuid=$(uuidgen)
      new=$(printf "$filename-$uuid.jpg")

      mv "$filename" "$new"
      let a=a+1
    done

    for filename in *.*g ; do
      new=$(printf "%04d.jpg" "$b")
      mv "$filename" "$new"

      echo "| OK -" $new
      let b=b+1
    done

    cd ../

  fi
done



